package com.massivecraft.massiveheads.engine;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.mixin.MixinMessage;
import com.massivecraft.massivecore.util.MUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class EngineHeadRightClick extends Engine
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static EngineHeadRightClick i = new EngineHeadRightClick();
    public static EngineHeadRightClick get() { return i; }

    // -------------------------------------------- //
    // EVENT
    // -------------------------------------------- //

    @EventHandler(priority = EventPriority.HIGHEST)
    public void playerInteractListener(PlayerInteractEvent event)
    {
        // PlayerInteractEvent is fired twice, once for the main hand, once for the off hand.
        // This ensures we only send the message once.
        if(! event.getHand().equals(EquipmentSlot.HAND)) return;

        if(MUtil.isntPlayer(event.getPlayer())) return;

        // Check we're clicking a player head.
        Block block = event.getClickedBlock();
        if(block == null || (! block.getType().equals(Material.SKULL))) return;

        // Get the name from the meta. Just use the plain string, since names can change, but the skull's name won't. Keep uniformity this way.
        Object[] drops = event.getClickedBlock().getDrops().toArray();
        if(! (drops[0] instanceof ItemStack)) return; // This shouldn't ever happen, just in case. todo clean up this entire section.

        // Ensure the itemstack is still a head.
        ItemStack itemStack = (ItemStack) drops[0];
        if(! itemStack.getType().equals(Material.SKULL_ITEM)) return; // Again, should never happen.

        SkullMeta meta = (SkullMeta) itemStack.getItemMeta();
        String ownerName = meta.getOwner();

        if(ownerName == null) return; // If it's a mob head

        // Inform the player.
        MixinMessage.get().msgOne(event.getPlayer(), "<i>This is the head of " + ownerName + ".");
    }
}