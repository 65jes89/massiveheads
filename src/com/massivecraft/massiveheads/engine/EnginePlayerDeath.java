package com.massivecraft.massiveheads.engine;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.PlayerUtil;
import com.massivecraft.massiveheads.entity.MConf;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class EnginePlayerDeath extends Engine
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static EnginePlayerDeath i = new EnginePlayerDeath();
    public static EnginePlayerDeath get() { return i; }

    // -------------------------------------------- //
    // EVENT
    // -------------------------------------------- //

    @EventHandler(priority = EventPriority.HIGHEST)
    public void playerDeath(PlayerDeathEvent event)
    {
        Player killed = event.getEntity();

        // Ensure that this is a valid death...
        if(MUtil.isntPlayer(killed)) return;
        if(PlayerUtil.isDuplicateDeathEvent(event)) return;

        // Get the damage cause...
        EntityDamageEvent damageEvent = killed.getLastDamageCause();

        // Get the killer. Null if the killer isn't an entity.
        Entity killer = MUtil.getLiableDamager(damageEvent);

        // Drop head
        if(MConf.get().shouldDropHead(killer)) dropPlayerHead(killed);
    }

    // Spawn head... todo move to util class?
    private void dropPlayerHead(Player killed)
    {
        // Make the skull
        ItemStack playerHead = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta meta = (SkullMeta) playerHead.getItemMeta();
        meta.setOwner(killed.getName());
        playerHead.setItemMeta(meta);

        killed.getWorld().dropItemNaturally(killed.getLocation(), playerHead);
    }
}