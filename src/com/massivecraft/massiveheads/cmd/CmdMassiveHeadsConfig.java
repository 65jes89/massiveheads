package com.massivecraft.massiveheads.cmd;

import com.massivecraft.massivecore.command.editor.CommandEditSingleton;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import com.massivecraft.massiveheads.Perm;
import com.massivecraft.massiveheads.entity.MConf;

import java.util.List;

public class CmdMassiveHeadsConfig extends CommandEditSingleton<MConf>
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static CmdMassiveHeadsConfig i = new CmdMassiveHeadsConfig();
    public static CmdMassiveHeadsConfig get() { return i; }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdMassiveHeadsConfig()
    {
        super(MConf.get()); // Takes care of the config command behavior for us

        // Requirements
        this.addRequirements(RequirementHasPerm.get(Perm.CONFIG));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesMassiveHeadsConfig;
    }
}
