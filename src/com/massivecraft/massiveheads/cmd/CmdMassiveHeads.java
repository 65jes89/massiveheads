package com.massivecraft.massiveheads.cmd;

import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import com.massivecraft.massiveheads.Perm;
import com.massivecraft.massiveheads.entity.MConf;

import java.util.List;

public class CmdMassiveHeads extends MassiveCommand
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static CmdMassiveHeads i = new CmdMassiveHeads();
    public static CmdMassiveHeads get() { return i; }

    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //

    private CmdMassiveHeadsConfig cmdMassiveHeadsConfig = new CmdMassiveHeadsConfig();
    private CmdMassiveHeadsVersion cmdMassiveHeadsVersion = new CmdMassiveHeadsVersion();

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdMassiveHeads()
    {
        this.addChild(cmdMassiveHeadsConfig);
        this.addChild(cmdMassiveHeadsVersion);

        this.addRequirements(RequirementHasPerm.get(Perm.BASECOMMAND));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesMassiveHeads;
    }
}
