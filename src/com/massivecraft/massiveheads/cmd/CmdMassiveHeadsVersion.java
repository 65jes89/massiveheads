package com.massivecraft.massiveheads.cmd;

import com.massivecraft.massivecore.command.MassiveCommandVersion;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import com.massivecraft.massiveheads.MassiveHeads;
import com.massivecraft.massiveheads.Perm;
import com.massivecraft.massiveheads.entity.MConf;

import java.util.List;

public class CmdMassiveHeadsVersion extends MassiveCommandVersion
{
    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdMassiveHeadsVersion()
    {
        super(MassiveHeads.get());

        // Requirements
        this.addRequirements(RequirementHasPerm.get(Perm.VERSION));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesMassiveHeadsVersion;
    }
}