package com.massivecraft.massiveheads.entity;

import com.massivecraft.massivecore.collections.MassiveList;
import com.massivecraft.massivecore.collections.MassiveMap;
import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.IntervalUtil;
import com.massivecraft.massivecore.util.PermissionUtil;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.util.List;
import java.util.Map;

public class MConf extends Entity<MConf>
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    protected static transient MConf i;
    public static MConf get() { return i; }

    // -------------------------------------------- //
    // COMMAND ALIASES
    // -------------------------------------------- //

    public List<String> aliasesMassiveHeads = new MassiveList<>("massiveheads", "heads");
    public List<String> aliasesMassiveHeadsConfig = new MassiveList<>("config");
    public List<String> aliasesMassiveHeadsVersion = new MassiveList<>("version", "v");

    private int defaultHeadDropChance = 1;
    private Map<Permission, Integer> headDropChances = new MassiveMap<>(PermissionUtil.asPermission("massiveheads.premium_headdrop"), 5,
            PermissionUtil.asPermission("massiveheads.supremium_headdrop"), 10);

    // -------------------------------------------- //
    // GETTERS
    // -------------------------------------------- //

    public boolean shouldDropHead(org.bukkit.entity.Entity entity) // todo rework to use permissions map
    {
        // If not killed by a player, don't drop the head.
        if(! (entity instanceof Player)) return false;

        int chance = defaultHeadDropChance;
        for(Map.Entry<Permission, Integer> i : headDropChances.entrySet())
        {
            if(! PermissionUtil.hasPermission(entity, i.getKey())) continue;
            if(i.getValue() > chance) chance = i.getValue();
        }

        int num = IntervalUtil.random(1, 100); // todo make sure this is inclusive
        return num <= chance;
    }
}