package com.massivecraft.massiveheads;

import com.massivecraft.massivecore.Identified;
import com.massivecraft.massivecore.util.PermissionUtil;

public enum Perm implements Identified
{
    // -------------------------------------------- //
    // ENUM
    // -------------------------------------------- //
    BASECOMMAND,
    CONFIG,
    VERSION,
    PREMIUM_HEADDROP,
    SUPREMIUM_HEADDROP
    // END OF LIST
    ;

    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //

    private final String id;

    @Override
    public String getId()
    {
        return this.id;
    }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    Perm()
    {
        this.id = PermissionUtil.createPermissionId(MassiveHeads.get(), this);
    }
}