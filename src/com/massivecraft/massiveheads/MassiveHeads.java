package com.massivecraft.massiveheads;

import com.massivecraft.massivecore.MassivePlugin;
import com.massivecraft.massiveheads.cmd.CmdMassiveHeads;
import com.massivecraft.massiveheads.engine.EngineHeadRightClick;
import com.massivecraft.massiveheads.engine.EnginePlayerDeath;
import com.massivecraft.massiveheads.entity.MConfColl;

public class MassiveHeads extends MassivePlugin
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static MassiveHeads i;
    public static MassiveHeads get() { return i; }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public MassiveHeads() { MassiveHeads.i = this; }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public void onEnableInner()
    {

        this.activate(
                // Engines
                EnginePlayerDeath.class,
                EngineHeadRightClick.class,

                // Entities
                MConfColl.class,

                // Commands
                CmdMassiveHeads.class
        );
    }
}